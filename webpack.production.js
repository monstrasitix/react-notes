// Core
const webpackMerge = require('webpack-merge');
const webpackCommon = require('./webpack.common');


/**
 * Webpack should handle production bundles as it's latest version is optimized to handle
 * requireements out of the box.
 * @module webpack/production
 */


module.exports = webpackMerge(webpackCommon, {
    mode: 'production',
});
