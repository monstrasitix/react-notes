// Core
const path = require('path');
const webpackMerge = require('webpack-merge');

// Common
const webpackCommon = require('./webpack.common');

module.exports = webpackMerge(webpackCommon, {
    mode: 'development',
    devServer: {
        port: 9000,
        inline: true,
        publicPath: '/',
        historyApiFallback: true,
        contentBase: path.join(__dirname, './bunles'),
    },
    devtool: 'inline-source-map',
});