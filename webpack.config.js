/**
 * Main webpack configuration utilizes `--env` flag which
 * is used to require different configuration files for specific environments.
 * You can note various configuration files within the root directory. Those extension
 * names need to match env argument in order to run bundling successfuly.
 * @module webpack
 */
module.exports = environment => (
    // EsLint: Unexpected `require statement`
    require(`./webpack.${environment}.js`) // eslint-disable-line
);