# Note Client

Simple note editor utilizing Note REST API for data perostance
and Draft.js framework for a WYSIWYG editor to compose those notes. - That's
ma assignment.

## Notes to consider

- React-Create-App was not used to build this environment. Everything was manual as expected.
- All typo errors were intentional.
- Basic Auth code is stored inside `./secret.json` in `{ "code": "username:password" }` format.
- Secret is not persisted within Git and that secret JSON file is passed in through webpack.
- I could not run Docker service because my OS does not support virtualization and I decided to build my own Server with MongoDB and Hapi.js.
- Not everything is documented and tested.
- Flow exists but I have not used it.

## Directory structure

Main application resides within `./src` directory and I would like to explain where is what and how how, how!

- `./src/actions` - Core mappings between redux's state and middleware. (Contains action creators "2").
- `./src/assets` - Contains compiled SASS
- `./src/components` - Contains functional components with state but contain no Rrdux bindings.
- `./src/config` - Contains note's API configuration in relation to Authentication and resource URL.
- `./src/containers` - Components with Redux bindings + HOCs.
- `./src/modules` - Contains badly abstracted Draft.js editor buttons and fonts.
- `./src/reducers` - Front-end's backend...
- `./src/sagas` - Redux middleware which deals with side effects in relation to Note's API
- `./src/selectors` - State accessors.
- `./app.react` - Root of the application
- `./environment` - Where environment gets decided
- `./history` - Custom History for React-Router-DOM
- `./store` - Redux configuration with middleware

## CLI Usage

All commands need to be executed via `npm run <script>` or `yarn <script>` (Not sure about yarn).
Persuming Node.js was installed with NPM being selected during installation.

- `webpack.development` - Run's development server with `webpack-dev-server`
- `webpack.production` - Generates production bundle
- `document.gen` - Generates static documentation with JSDOC
- `document.open` - Opens JSDOC's documentation
- `test` - Run's tests with Mocha and Chai
- `test.results` - Shows test documentation
- `test.coverage` - Shows test coverage


## Webpack configuration

Webpack is configured in 4 separate files outlined below:

- webpack.config.js
- webpack.common.js
- webpack.development.js
- webpack.production.js

It is important to note file extension as those names are refered within `package.json` as
environments for which script should be ran to persume a specific environment.

Core execution initiates inside the main cofngiuration file and common implementations reside in common.
You can read more about webpack's configuration while redering to JSDOC's generated documentation
which outlines their functionality in detail.

## Testing

For testing I have used Mocha as my test runner and Chai as my assertion library. There's one command
that runs all tests and preforms static documentation generation for completed procedures.

## Documentation

I went pretty vague on this one... It is not finished andbecause I persumed it would not be essential
for this assignment but it does look pretty. - Like hence my writing and it's obvious that I was not in
the mood for it. Not from a bad perspective it's just I've done a lot of it recently.

Now Dima is rambling again. Sorry!