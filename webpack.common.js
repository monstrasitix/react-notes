// Core
const Path = require('path');
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');

// Secret
const SECRET = require('./secret.json'); // { code: '{username}:{password}' }


/**
 * Common bundling configurations are outliner here and will take effect per each environment.
 * That includes bundling styles and react components under certain loaders and correct
 * paths. - It is also important to note that I am using webpack to provide API's code
 * for basic authentication which will be injected as global for all environments.
 * @module webpack/common
 */


/**
 * Provides secret code which will be encoded in Base64, This value will be
 * used to access note REST API.
 * @memberof module:webpack/common
 * @type {Plugin}
 * @constant
 */
const definitionPlugin = new webpack.DefinePlugin({
    CODE: JSON.stringify(Buffer.from(SECRET.code).toString('base64')),
});


/**
 * HTML markup will be provided, and last bundle will be injected appropriately into
 * destined source found in `./dist` directory. - This allows me to organize CDN
 * imports and let webpack manage my bundle source for production and for my development server.
 * @memberof module:webpack/common
 * @type {Plugin}
 * @constant
 */
const htmlTemplatePlugin = new HtmlWebPackPlugin({
    filename: './index.html',
    template: './src/index.html',
});


module.exports = {
    entry: {
        app: [
            '@babel/polyfill',
            Path.join(__dirname, './src/app.react.jsx')
        ],
    },
    module: {
        rules: [{
                test: /\.sass$/i,
                exclude: /node_modules/i,
                loader: 'style-loader!css-loader!sass-loader',
            },
            {
                test: /\.jsx?/i,
                loader: 'babel-loader',
                exclude: /node_modules/i,
            },
        ],
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    plugins: [
        definitionPlugin,
        htmlTemplatePlugin,
    ],
};