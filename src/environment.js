/**
 * Environment specific functionality provides determenistic ways to decide
 * between environments.
 * @module environment
 */


/**
 * Provided string gets tested against regexes to determine environment from
 * various sources. This allows for flexability and later use requires conditional comparisson
 * against object's properties.
 * @memberof module:environment
 * @constant
 * @function
 * @param {String} env - Environment
 * @returns {Object} Boolean properties for specified environments
 * @example
 * let env = getEnvironment('production');
 * env = getEnvironment('prod');
 * env = getEnvironment('PrOd');
 * // env.PRODUCTION === true
 */
export const getEnvironment = env => ({
    TESTING: /test/i.test(env),
    PRODUCTION: /prod/i.test(env),
    DEVELOPMENT: /dev/i.test(env),
});

export default getEnvironment(process.env.NODE_ENV);