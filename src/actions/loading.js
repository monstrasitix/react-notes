/**
 * Loading actions are used to alter loading properties within state.
 * @module actions/loading
 */


/**
 * Loading action is used to modify certain property's value on demand.
 * @memberof module:actions/loading
 * @constant
 * @type {Object}
 * @property {String} LOADING_REPLACE - Replaces entity's value
 */
export const ACTION = {
    LOADING_REPLACE: 'LOADING_REPLACE',
};


/**
 * Used to alter loading state for a prticular property on the loading branch within
 * state.
 * @memberof module:actions/loading
 * @constant
 * @function
 * @param {String} entity - Property to update
 * @param {Boolean} value - Boolean value to change to
 * @returns {Object} Action
 */
export const loadingReplace = (entity, value) => ({
    type: ACTION.LOADING_REPLACE,
    payload: value,
    meta: entity,
});