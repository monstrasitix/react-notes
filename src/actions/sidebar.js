/**
 * Sidebar toggle state alterations can be triggered via calling these
 * action creators and actions can be mapped to reducers.
 * @module actions/sidebar
 */


/**
 * Actions which map directly to redux without traversing middleware channels.
 * @memberof module:actions/sidebar
 * @constant
 * @type {Object}
 * @property {String} SIDEBAR_TOGGLE - Sidebar toggling
 */
export const ACTION = {
    SIDEBAR_TOGGLE: 'SIDEBAR_TOGGLE',
};


/**
 * Alter's sidebar's toggle value.
 * @memberof module:actions/sidebar
 * @constant
 * @function
 * @param {Boolean} isOpen - New value for sidebar's toggle value
 * @returns {Object} Action
 */
export const toggleSidebar = isOpen => ({
    type: ACTION.SIDEBAR_TOGGLE,
    payload: isOpen,
});