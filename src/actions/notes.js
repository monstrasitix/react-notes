/**
 * Redux and middleware interactions for note entity reside within this module.
 * There is adistinction between action and saga. Both traverse the store for reducers,
 * but saga preforms side effects on every dispatch and can chain other sagas. In this case there is
 * no chaining and voer of this implementation is used for HTTP requests.
 * @module actions/notes
 */


/**
 * These actions are mapped to reducers which take effect after the middlware.
 * Whenever something was laoded, created or updated data flows from the external source
 * and into state. There are no assumptions regarding provided state is correct. And information
 * is always derived from the resource.
 * @memberof module:actions/notes
 * @constant
 * @type {Object}
 * @property {String} NOTES_REPLACE - Replaces whole note collection
 * @property {String} NOTE_REPLACE - Replaces single note during update
 * @property {String} NOTE_ADD - Adds a new note to the store
 */
export const ACTION = {
    NOTES_REPLACE: 'NOTES_REPLACE',
    NOTE_REPLACE: 'NOTE_REPLACE',
    NOTE_ADD: 'NOTE_ADD',
};


/**
 * These actions are bound to middleware which allow me to preform side effects. There is no abstraction
 * around the resource itself, which would be useful. (Also if it would be injected as a dependency.)
 * These actions utilize the REST API and further propagation forwards derived values into application's
 * state.
 * @memberof module:actions/notes
 * @constant
 * @type {Object}
 * @property {String} LOAD_NOTES - Fetches notes from a REST API
 * @property {String} SAVE_NOTE - Updates provided note
 * @property {String} CREATE_NOTE - Creates a new note
 */
export const SAGA = {
    LOAD_NOTES: 'LOAD_NOTES',
    SAVE_NOTE: 'SAVE_NOTE',
    CREATE_NOTE: 'CREATE_NOTE',
};


export const notesReplace = notes => ({
    type: ACTION.NOTES_REPLACE,
    payload: notes
});

export const noteReplace = (id, note) => ({
    type: ACTION.NOTE_REPLACE,
    payload: note,
    meta: id
});

export const noteAdd = note => ({
    type: ACTION.NOTE_ADD,
    payload: note,
});

export const loadNotes = () => ({
    type: SAGA.LOAD_NOTES
});

export const saveNote = (id, note) => ({
    type: SAGA.SAVE_NOTE,
    id,
    note,
});

export const createNote = note => ({
    type: SAGA.CREATE_NOTE,
    note,
});