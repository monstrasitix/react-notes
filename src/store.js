import {
    createStore,
    combineReducers,
    applyMiddleware
} from 'redux';
import ReduxSaga from 'redux-saga';
import {
    createLogger
} from 'redux-logger';

// Environment
import environment from './environment';

// Reducers
import reducers from './reducers';

// Sagas
import sagas from './sagas';


/**
 * Store module initializes Redux with Redux-Saga middleware.
 * There are many ways to initialize Redux and many places to optimize it for multiple
 * environments. For this use case it is fair to leave it here and register middleware.
 * I also chose redux-Saga because I am in favor of channel isolation that Redux-Thunk does not
 * have. Side effects can be managed in a more abstract sence as component dispatches are not
 * aware of whate xactly will happen and there are only expectations.
 * @module store
 */


/**
 * Contains middleware with object data structure. This is convenient because
 * each registered middleware can be accessed via property and can be removed
 * depending on the environment. - Initially all middleware that belong to this
 * object will be registered. Propritizing order matters here because logger will log
 * middleware dispatches as well as redux actions if it comes first.
 * @memberof module:store
 * @constant
 * @type {Object}
 */
const middleware = {
    saga: ReduxSaga(),
};


if (environment.DEVELOPMENT) {
    middleware.logger = createLogger({
        collapsed: true
    });
}


/**
 * Main store which is provided to React-Redux to handle state updates with store subscriptions.
 * That will allow specific component abstractions to take use of dispatch capability in relation
 * to props and rerendering.
 * @memberof module:store
 * @type {Object}
 */
const store = createStore(
    combineReducers(reducers),
    applyMiddleware(...Object.values(middleware)),
);


// Needs to be envoked after store's creation
middleware.saga.run(sagas);


export default store;