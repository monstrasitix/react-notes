// Core
import React from 'react';
import PropTypes from 'prop-types';


const Icon = ({ type, className, ...rest }) => (
    <i {...rest} className={`fa fa-${type} ${className}`} />
);


Icon.propTypes = {
    type: PropTypes.string,
    className: PropTypes.string,
};


Icon.defaultProps = {
    type: 'leaf',
    className: '',
};


export default Icon;