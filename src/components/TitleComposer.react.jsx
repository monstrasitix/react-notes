// Core
import React from 'react';
import PropTypes from 'prop-types';
import { Editor, EditorState } from 'draft-js';

const TitleComposer = ({ editorState, onChange }) => (
    <Editor
        onChange={onChange}
        editorState={editorState}
        placeholder="Enter title"
    />
);


TitleComposer.propTypes = {
    onChange: PropTypes.func.isRequired,
    editorState: PropTypes.object,
};


TitleComposer.defaultProps = {
    editorState: EditorState.createEmpty(),
};


export default TitleComposer;