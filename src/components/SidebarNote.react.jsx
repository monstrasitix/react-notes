// Core
import React from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { convertFromRaw } from 'draft-js';

const conversion = (value) => {
    try {
        const parsed = JSON.parse(value);
        return convertFromRaw(parsed).getPlainText();
    } catch (error) {
        return value;
    }
};

const SidebarNote = ({ to, title }) => (
    <li className="nav-item">
        <NavLink to={to} className="nav-link d-block sidebar__link p-2">
            {conversion(title)}
        </NavLink>
    </li>
);

SidebarNote.propTypes = {
    to: PropTypes.string.isRequired,
    title: PropTypes.string,
};

SidebarNote.defaultProps = {
    title: 'Empty note',
};


export default SidebarNote;