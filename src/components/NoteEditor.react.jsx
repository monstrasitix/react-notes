// Core
import React from 'react';
import PropTypes from 'prop-types';
import { EditorState, ContentState, convertFromRaw, convertToRaw } from 'draft-js';

// Components
import Composer from './Composer.react';
import Icon from './Icon.react';
import TitleComposer from './TitleComposer.react';

class NoteEditor extends React.Component {
    static setEditorValue(value) {
        try {
            const parsed = JSON.parse(value);
            return EditorState.createWithContent(convertFromRaw(parsed));
        } catch (error) {
            return EditorState.createWithContent(ContentState.createFromText(value));
        }
    }

    constructor(props) {
        super(props);

        const { note, title } = this.props;

        this.state = {
            titleChanged: true,
            noteChanged: true,
            titleState: NoteEditor.setEditorValue(title),
            noteState: NoteEditor.setEditorValue(note),
        };

        this.onSave = this.onSave.bind(this);
        this.getTitleChange = this.getTitleChange.bind(this);
        this.getNoteChange = this.getNoteChange.bind(this);
    }

    componentWillReceiveProps({ title, note }) {
        this.setState(() => ({
            titleState: NoteEditor.setEditorValue(title),
            noteState: NoteEditor.setEditorValue(note),
            titleChanged: true,
            noteChanged: true,
        }));
    }

    onSave() {
        const { saveNote, id } = this.props;
        const { titleState, noteState } = this.state;
        saveNote(id, {
            title: JSON.stringify(convertToRaw(titleState.getCurrentContent())),
            note: JSON.stringify(convertToRaw(noteState.getCurrentContent())),
        });
    }

    getNoteChange(noteState) {
        const { note } = this.props;
        this.setState(() => ({
            noteState,
            noteChanged: JSON.stringify(convertToRaw(noteState.getCurrentContent())) === note,
        }));
    }


    getTitleChange(titleState) {
        const { title } = this.props;
        this.setState(() => ({
            titleState,
            titleChanged: JSON.stringify(convertToRaw(titleState.getCurrentContent())) === title,
        }));
    }

    render() {
        const { noteChanged, titleChanged, noteState, titleState } = this.state;
        return (
            <div className="container">
                <div className="row mt-5 mb-3">
                    <div className="col-12">
                        <div className="col-12">
                            <h1>
                                <TitleComposer
                                    editorState={titleState}
                                    onChange={this.getTitleChange}
                                />
                            </h1>
                        </div>
                    </div>
                </div>

                <div className="row mb-5">
                    <div className="col-12">
                        <div className="card">
                            <div className="card-body">
                                <Composer
                                    editorState={noteState}
                                    onChange={this.getNoteChange}
                                    placeholder="Note ..."
                                />
                            </div>
                            <div className="card-footer">
                                <button onClick={this.onSave} disabled={titleChanged && noteChanged} type="button" className="float-right btn-circle btn btn-lg btn-primary rounded-circle">
                                    <Icon type="save" />
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


NoteEditor.propTypes = {
    note: PropTypes.string,
    title: PropTypes.string,
    id: PropTypes.any.isRequired,
    saveNote: PropTypes.func.isRequired,
};

NoteEditor.defaultProps = {
    title: '',
    note: '',
};

export default NoteEditor;