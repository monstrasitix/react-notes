// Core
import React from 'react';
import PropTypes from 'prop-types';

// Components
import DropdownButton from './DropdownButton.react';

class DropdownToggle extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            children: this.props.children, // eslint-disable-line
        };

        this.onChange = this.onChange.bind(this);
    }

    onChange(item) {
        this.setState(() => ({ children: item.text }));
        this.props.onChange(item); // eslint-disable-line
    }

    render() {
        const { children } = this.state;
        return (
            <DropdownButton
                {...this.props}
                onChange={this.onChange}
            >
                {children}
            </DropdownButton>
        );
    }
}

DropdownToggle.propTypes = {
    children: PropTypes.node,
    onChange: PropTypes.func.isRequired,
};

DropdownToggle.defaultProps = {
    children: null,
};

export default DropdownToggle;