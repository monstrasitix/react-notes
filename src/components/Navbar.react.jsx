// Core
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Navbar = ({ onToggle, match }) => (
    <nav className="navbar navbar-light bg-white">
        <button onClick={onToggle} className="navbar-toggler border-0" type="button">
            <span className="fa fa-bars" />
        </button>
        <Link to={match.path} className="navbar-brand">Notes</Link>
    </nav>
);

Navbar.propTypes = {
    onToggle: PropTypes.func.isRequired,
    match: PropTypes.shape({
        path: PropTypes.string,
    }).isRequired,
};

export default Navbar;