// Core
import React from 'react';
import PropTypes from 'prop-types';

const Information = ({ children }) => (
    <p className="text-muted text-center">
        {children}
    </p>
);


Information.propTypes = {
    children: PropTypes.node,
};


Information.defaultProps = {
    children: null,
};


export default Information;