// Core
import React from 'react';
import PropTypes from 'prop-types';

// Components
import Icon from './Icon.react';
import ListState from './ListState.react';
import Information from './Information.react';
import SidebarNote from './SidebarNote.react';

const Sidebar = ({ match, notes, createNote, loading, isOpen }) => (
    <aside className="sidebar background background__mountain scroll scroll-y" style={Sidebar.OPEN_STATE[isOpen]}>
        <nav className="navbar navbar-dark">
            <ul className="navbar-nav flex-column">
                <li className="nav-item">
                    <button type="button" onClick={createNote} className="btn btn-link text-white">
                        <Icon type="plus-circle" />
                        <span className="pl-2">Add note</span>
                    </button>
                </li>
            </ul>
        </nav>
        <nav className="navbar navbar-dark p-0">
            <ListState
                loading={loading}
                empty={notes.length < 1}
                renderEmpty={<Information children="No notes" />}
                renderLoading={<Information children="Loading notes" />}
            >
                <ul className="navbar-nav flex-column w-100">
                    {
                        notes.map(({ id, title }) => (
                            <SidebarNote key={id} to={`${match.url}/${id}`} title={title} />
                        ))
                    }
                </ul>
            </ListState>
        </nav>
    </aside>
);


Sidebar.OPEN_STATE = {
    true: { maxWidth: '250px', minWidth: '250px' },
    false: { minWidth: 0, width: 0 },
};


Sidebar.propTypes = {
    notes: PropTypes.arrayOf(PropTypes.shape({
        id: PropTypes.any,
        title: PropTypes.string,
    })),
    match: PropTypes.shape({
        url: PropTypes.string,
    }).isRequired,
    createNote: PropTypes.func.isRequired,
    loading: PropTypes.bool,
    isOpen: PropTypes.bool,
};


Sidebar.defaultProps = {
    notes: [],
    loading: false,
    isOpen: true,
};

export default Sidebar;