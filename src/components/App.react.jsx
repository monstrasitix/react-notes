// Core
import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Route } from 'react-router-dom';

// Components
import Initial from './Initial.react';

// Containers
import NavbarContainer from '../containers/NavbarContainer.react';
import SidebarContainer from '../containers/SidebarContainer.react';
import NoteEditorContainer from '../containers/NoteEditorContainer.react';


const App = ({ match }) => (
    <div className="d-flex vh-100">
        <Route component={SidebarContainer} />

        <div className="bg-light w-100 d-flex flex-column">
            <Route component={NavbarContainer} />

            <main className="h-100 scroll scroll-y">
                <Switch>
                    <Route path={`${match.path}/:noteId`} component={NoteEditorContainer} />
                    <Route component={Initial} />
                </Switch>
            </main>
        </div>
    </div>
);


App.propTypes = {
    match: PropTypes.shape({
        path: PropTypes.string,
    }).isRequired,
};


export default App;