// Core
import React from 'react';
import PropTypes from 'prop-types';
import { Editor, EditorState, RichUtils } from 'draft-js';

// Components
import Icon from './Icon.react';
import DropdownToggle from './DropdownToggle.react';

// Modules
import { fontMap, fontValues, headingValues } from '../modules/editor';

class Composer extends React.Component {
    constructor(props) {
        super(props);


        this.buttonClassName = 'btn btn-link text-muted btn-sm';

        this.toggleInline = this.toggleInline.bind(this);
        this.toggleBlock = this.toggleBlock.bind(this);
        this.onToggleHeading = this.onToggleHeading.bind(this);
        this.onToggleFont = this.onToggleFont.bind(this);

        this.onBoldClick = this.toggleInline('BOLD');
        this.onItalicClick = this.toggleInline('ITALIC');
        this.onUnderlineClick = this.toggleInline('UNDERLINE');

        this.onBlockquoteClick = this.toggleBlock('blockquote');

        this.onUnorderedClick = this.toggleBlock('unordered-list-item');
        this.onOrderedClick = this.toggleBlock('ordered-list-item');
        this.onCodeBlockClick = this.toggleBlock('code-block');
    }

    onToggleHeading({ value }) {
        const { onChange, editorState } = this.props;
        onChange(RichUtils.toggleBlockType(editorState, value));
    }

    onToggleFont({ value }) {
        const { onChange, editorState } = this.props;
        onChange(RichUtils.toggleInlineStyle(editorState, value));
    }


    toggleInline(type) {
        return () => {
            const { onChange, editorState } = this.props;
            onChange(RichUtils.toggleInlineStyle(editorState, type));
        };
    }

    toggleBlock(type) {
        return () => {
            const { onChange, editorState } = this.props;
            onChange(RichUtils.toggleBlockType(editorState, type));
        };
    }

    render() {
        const { placeholder, editorState, onChange } = this.props;
        return (
            <div>
                <div className="btn-group">
                    <button type="button" className={this.buttonClassName} onClick={this.onBoldClick}>
                        <Icon type="bold" />
                    </button>
                    <button type="button" className={this.buttonClassName} onClick={this.onItalicClick}>
                        <Icon type="italic" />
                    </button>
                    <button type="button" className={this.buttonClassName} onClick={this.onUnderlineClick}>
                        <Icon type="underline" />
                    </button>
                </div>

                <div className="btn-group">
                    <DropdownToggle
                        items={headingValues}
                        className={this.buttonClassName}
                        onChange={this.onToggleHeading}
                    >
                        {headingValues[0].text}
                    </DropdownToggle>
                    <DropdownToggle
                        items={fontValues}
                        className={this.buttonClassName}
                        onChange={this.onToggleFont}
                    >
                        {fontValues[0].text}
                    </DropdownToggle>
                </div>

                <div className="btn-group mx-2">
                    <button type="button" className={this.buttonClassName} onClick={this.onBlockquoteClick}>
                        <Icon type="quote-right" />
                    </button>
                    <button type="button" className={this.buttonClassName} onClick={this.onCodeBlockClick}>
                        <Icon type="code" />
                    </button>
                </div>

                <div className="btn-group mx-2">
                    <button type="button" className={this.buttonClassName} onClick={this.onUnorderedClick}>
                        <Icon type="list-ul" />
                    </button>
                    <button type="button" className={this.buttonClassName} onClick={this.onOrderedClick}>
                        <Icon type="list-ol" />
                    </button>
                </div>

                <hr />

                <Editor
                    onChange={onChange}
                    editorState={editorState}
                    customStyleMap={fontMap}
                    placeholder={placeholder}
                />
            </div>
        );
    }
}

Composer.propTypes = {
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    editorState: PropTypes.object,
};
Composer.defaultProps = {
    placeholder: '',
    editorState: EditorState.createEmpty(),
};

export default Composer;