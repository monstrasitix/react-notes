// Core
import PropTypes from 'prop-types';

const ListState = ({ renderLoading, renderEmpty, loading, empty, children }) => {
    switch (true) {
        case loading:
            return renderLoading;
        case empty:
            return renderEmpty;
        default:
            return children;
    }
};

ListState.propTypes = {
    empty: PropTypes.bool,
    loading: PropTypes.bool,
    children: PropTypes.node,
    renderEmpty: PropTypes.node,
    renderLoading: PropTypes.node,
};

ListState.defaultProps = {
    empty: false,
    loading: false,
    children: null,
    renderEmpty: null,
    renderLoading: null,
};


export default ListState;