// Core
import React from 'react';
import PropTypes from 'prop-types';

// Components
import DropdownItem from './DropdownItem.react';

class DropdownButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };

        this.wrapperRef = undefined;

        this.toggleDropdown = this.toggleDropdown.bind(this);
        this.getReference = this.getReference.bind(this);
        this.handleClickOutside = this.handleClickOutside.bind(this);
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside, false);
    }

    componentWillUnmount() {
        document.addEventListener('mousedown', this.handleClickOutside, false);
    }

    getReference(ref) {
        this.wrapperRef = ref;
    }

    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.setState(() => ({ open: false }));
        }
    }

    toggleDropdown() {
        this.setState(oldState => ({ open: !oldState.open }));
    }

    render() {
        const { open } = this.state;
        const { className, children, items, onChange } = this.props;

        return (
            <div ref={this.getReference} className="dropdown">
                <button onClick={this.toggleDropdown} type="button" className={`dropdown-toggle ${className}`}>
                    {children}
                </button>
                <div className={`dropdown-menu ${open ? 'show' : ''}`}>
                    {
                        items.map(item => (
                            <DropdownItem key={item.value} onChange={() => onChange(item)}>
                                {item.text}
                            </DropdownItem>
                        ))
                    }
                </div>
            </div>
        );
    }
}


DropdownButton.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    onChange: PropTypes.func.isRequired,
    items: PropTypes.arrayOf(PropTypes.shape({
        text: PropTypes.node,
        value: PropTypes.string,
    })),
};


DropdownButton.defaultProps = {
    items: [],
    className: '',
    children: null,
};


export default DropdownButton;