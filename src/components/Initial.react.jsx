// Core
import React from 'react';

// Components
import Icon from './Icon.react';


const Initial = () => (
    <div className="d-flex align-items-center justify-content-center w-100 h-100">
        <Icon type="sticky-note" className="fa-5x text-grey" />
    </div>
);

export default Initial;