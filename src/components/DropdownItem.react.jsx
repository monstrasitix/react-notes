// Core
import React from 'react';
import PropTypes from 'prop-types';

const DropdownItem = ({ children, onChange }) => {
    const handleClick = (e) => {
        e.preventDefault();
        onChange();
    };
    return (
        <a onClick={handleClick} className="dropdown-item" href="#">
            {children}
        </a>
    );
};

DropdownItem.propTypes = {
    children: PropTypes.node,
    onChange: PropTypes.func.isRequired,
};


DropdownItem.defaultProps = {
    children: null,
};


export default DropdownItem;