// Core
import {
    effects as Effect
} from 'redux-saga';

// Configuration
import Config from '../config/resources';

// Actions
import {
    SAGA,
    notesReplace,
    noteReplace,
    noteAdd,
} from '../actions/notes';
import {
    loadingReplace
} from '../actions/loading';


// History
import History from '../history';


const headers = {
    'Authorization': `Basic ${Config.note.code}`,
};

export function* loadNotes() {
    yield Effect.put(loadingReplace('notes', true));
    try {
        const response = yield Effect.call(fetch, `${Config.note.url}/notes`, {
            method: 'GET',
            headers,
        });

        let notes = yield response.json();
        notes = notes.docs.map(note => ({
            id: note._id,
            title: note.title,
            note: note.note,
        }));


        yield Effect.put(notesReplace(notes));
    } catch (error) {
        console.error(error);
    } finally {
        yield Effect.put(loadingReplace('notes', false));
    }
}

export function* saveNote({ id, note }) {
    try {
        const response = yield Effect.call(fetch, `${Config.note.url}/notes/${id}`, {
            method: 'PUT',
            body: JSON.stringify(note),
            headers,
        });
        const result = yield response.json();
        result.id = id;
        delete result._id;

        yield Effect.put(noteReplace(id, result));
    } catch (error) {
        console.error(error);
    }
}

export function* addNote({ note }) {
    try {
        const response = yield Effect.call(fetch, `${Config.note.url}/notes`, {
            method: 'POST',
            body: JSON.stringify(note),
            headers,
        });
        const newNote = yield response.json();
        newNote.id = newNote._id;
        delete newNote._id;
        delete newNote.createdAt;
        yield Effect.put(noteAdd(newNote));

        History.push(`/notes/${newNote.id}`);
    } catch (error) {
        console.error(error);
    }
}

export default [
    Effect.takeEvery(SAGA.LOAD_NOTES, loadNotes),
    Effect.takeEvery(SAGA.SAVE_NOTE, saveNote),
    Effect.takeEvery(SAGA.CREATE_NOTE, addNote),
];