// Core
import { effects } from 'redux-saga';

// Sagas
import notes from './notes';

export default function* saga() {
    yield effects.all(notes);
}