import './assets/sass/main.sass';

// Core
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Switch, Route, Redirect } from 'react-router-dom';

// Components
import App from './components/App.react';

// Dependencies
import store from './store';
import history from './history';


/**
 * Main module is the starting point which renders whole application inside the specified
 * target. That target can be located inside `./src/index.html`. Note that for production
 * that template will be located inside `./dist` directory.
 * <br />
 * <br />
 * I am ergistering React-Redux provider as for React and Redux integration and React-Router-DOM
 * for layout organizing. Initially there is no structure and these routes will render appropriate
 * view whenever they are being landed on. This is a good time to provide base routes
 * to allow the application to build up from initial routes with `match` object provided by
 * the routing library.
 * @module application
 */


render(
    (
        <Router history={history}>
            <Provider store={store}>
                <Switch>
                    <Route path="/notes" component={App} />
                    <Route render={() => <Redirect to="/notes" />} />
                </Switch>
            </Provider>
        </Router>
    ),
    document.getElementById('root'),
);