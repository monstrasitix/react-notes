// Reducers
import notes from './notes';
import loading from './loading';
import sidebar from './sidebar';

export default {
    notes,
    loading,
    sidebar,
};