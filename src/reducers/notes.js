// Actions
import {
    ACTION
} from '../actions/notes';

export const initialState = [];

export const replaceMapper = action => note => (
    (note.id === action.meta)
        ? action.payload
        : note
);

export default (state = initialState, action) => {
    let newState;
    switch (action.type) {
        case ACTION.NOTES_REPLACE:
            newState = action.payload;
            break;
        case ACTION.NOTE_REPLACE:
            newState = state.map(replaceMapper(action));
            break;
        case ACTION.NOTE_ADD:
            newState = [
                ...state,
                action.payload
            ];
            break;
        default:
            newState = state;
            break;
    }
    return newState;
};