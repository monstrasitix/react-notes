// Actions
import {
    ACTION
} from '../actions/sidebar';

export const initialState = {
    isOpen: true,
};

export default (state = initialState, action) => {
    let newState;
    switch (action.type) {
        case ACTION.SIDEBAR_TOGGLE:
            newState = {
                ...state,
                isOpen: action.payload
            };
            break;
        default:
            newState = state;
    }
    return newState;
};