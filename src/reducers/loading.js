// Actions
import { ACTION } from '../actions/loading';

export const initialState = {
    notes: false,
};

export default (state = initialState, action) => {
    let newState;
    switch (action.type) {
        case ACTION.LOADING_REPLACE:
            newState = {
                ...state,
                [action.meta]: action.payload,
            };
            break;
        default:
            newState = state;
            break;
    }
    return newState;
};