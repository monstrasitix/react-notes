export const transformer = value => (
    `FONT:${value.toUpperCase().replace(/\s+/i, '_')}`
);

export const fontReducer = (acc, font) => {
    acc[transformer(font)] = font;
    return acc;
};

export const fonts = Array.prototype.reduce.call([
    'Arial',
    'Helvetica',
    'Monospace',
    'Times New Roman',
    'Times',
    'Courier New',
    'Courier',
    'Palatino',
    'Garamond',
    'Tahoma',
    'Bookman',
    'Avant Garde',
    'Verdana',
    'Georgia',
    'Comic Sans MS',
    'Trebuchet MS',
    'Arial Black',
    'Impact',
], fontReducer, {});


export const headingValues = [{
        value: 'paragraph',
        text: 'Normal'
    },
    {
        value: 'header-one',
        text: 'Heading 1'
    },
    {
        value: 'header-two',
        text: 'Heading 2'
    },
    {
        value: 'header-three',
        text: 'Heading 3'
    },
    {
        value: 'header-four',
        text: 'Heading 4'
    },
    {
        value: 'header-five',
        text: 'Heading 5'
    },
    {
        value: 'header-six',
        text: 'Heading 6'
    },
];

const fontEntries = Object.entries(fonts);

export const fontMap = fontEntries
    .reduce((acc, [key, value]) => ({
        ...acc,
        [key]: {
            fontFamily: value,
        }
    }), {});


export const fontValueMap = ([value, text]) => ({
    value,
    text
});


export const fontValues = fontEntries.map(fontValueMap);