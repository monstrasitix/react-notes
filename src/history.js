import createBrowserHistory from 'history/createBrowserHistory';


/**
 * History module gets provided to React-Router-DOM to provide specific
 * history configuration. These settings resemble `ReactRouterDOM.BrowserRouter`.
 * I like to do it this way so that I could alter routes within my Saga-Middleware.
 * @module history
 */


/**
 * Browser router allos me to provide user confirmation dialog interaction.
 * This function is a mere asbraction of the functionality to allow me to
 * inject the confirmation modal as a dependency.
 * @memberof module:history
 * @constant
 * @function
 * @param {Function} onConfirm - Confirmation callback
 */
export const getUserConfirmation = onConfirm => (
    (message, callback) => callback(onConfirm(message))
);


/**
 * Created history which can be imported in any file and used for route's state alteration.
 * This object provide's History API within middleware.
 * @memberof module:history
 * @constant
 * @type {Object}
 */
export default createBrowserHistory({
    basename: '',
    keyLength: 6,
    forceRefresh: false,
    getUserConfirmation: getUserConfirmation(window.confirm),
});