// Core
import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

// Actions
import { loadNotes, createNote } from '../actions/notes';

// Selectors
import { getNotes } from '../selectors/notes';
import { areNotesLoading } from '../selectors/loading';
import { isSidebarOpen } from '../selectors/sidebar';

// Components
import Sidebar from '../components/Sidebar.react';


export const mapStateToProps = state => ({
    notes: getNotes(state),
    isOpen: isSidebarOpen(state),
    loading: areNotesLoading(state),
});


export const mapDispatchToProps = dispatch => ({
    loadNotes: () => dispatch(loadNotes()),
    createNote: () => dispatch(createNote({
        title: 'Empty note',
        note: '',
    })),
});


export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    lifecycle({
        componentDidMount() {
            this.props.loadNotes();
        },
    }),
)(Sidebar);