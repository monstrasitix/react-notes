// Core
import { connect } from 'react-redux';

// Components
import NoteEditor from '../components/NoteEditor.react';

// Selectors
import { getNotes } from '../selectors/notes';

// Actions
import { saveNote } from '../actions/notes';


export const mapStateToProps = (state, ownProps) => {
    const noteId = ownProps.match.params.noteId; // parseFloat(, 10);
    return getNotes(state).find(note => note.id === noteId) || {};
};


export const mapDispatchToProps = dispatch => ({
    saveNote: (id, note) => dispatch(saveNote(id, note)),
});


export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(NoteEditor);