// Core
import { connect } from 'react-redux';
import { compose, withProps } from 'recompose';

// Actions
import { toggleSidebar } from '../actions/sidebar';

// Selectors
import { isSidebarOpen } from '../selectors/sidebar';


// Components
import Navbar from '../components/Navbar.react';


export const mapStateToProps = state => ({
    isOpen: isSidebarOpen(state),
});

export const mapDispatchToProps = dispatch => ({
    onToggle: isOpen => dispatch(toggleSidebar(!isOpen))
});

export default compose(
    connect(
        mapStateToProps,
        mapDispatchToProps,
    ),
    withProps(props => ({
        onToggle: () => props.onToggle(props.isOpen),
    })),
)(Navbar);