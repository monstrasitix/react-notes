/**
 * These selectors are responsible to accessing note's state. I admint that the data structure
 * is not normalized as it could be by convention.
 * @module selectors/notes
 */


/**
 * Returns all notes found inside application's state.
 * @memberof module:selectors/notes
 * @constant
 * @function
 * @param {Object} state - Application's state
 * @returns {Object[]} Collection of objects
 */
export const getNotes = state => state.notes;