/**
 * Sidebar's state could be accessed via component's reference but that would be dirty.
 * I persist that value within state to provide access to it to components that might interact
 * with the sidebar. There could be a component that toggles the value whenever it is pressed. Another
 * feature which is not implemented, but could be. Imagine sidebar toggling outomatically whenever
 * a certain action has been preformed or on different screen sizes. That could be isolated from the
 * sidebar and core resizing functionality could be rendered elsewhere when particular lifecycle hooks
 * are important for this feauture.
 * @module selectors/sidebar
 */


/**
 * Provides sidebar's toggle state.
 * @memberof module:selectors/sidebar
 * @constant
 * @function
 * @param {Object} state - Application's state
 * @returns {Boolean} Is sidebar toggled
 */
export const isSidebarOpen = state => state.sidebar.isOpen;