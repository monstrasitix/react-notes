/**
 * Loading state is separate from their entities to allow less updates and other features.
 * There could be scenarios where it would be required to show a general load identification
 * in the UI when something is being loaded, no matter what. Then it would be easy to fetch all
 * loading attributes and check if either poperty evaluaetes to true. And while that remains so
 * UI could have other implementations with this separation.
 * @module selectors/loading
 */


/**
 * Whenever notes are being fetched from an API there should be a dispatch indicating
 * load state for notes. With that action, middleware should propagate to alter that value
 * for particular use cases. - This selector will return specifically that value from whole
 * state.
 * @memberof module:selectors/loading
 * @constant
 * @function
 * @param {Object} state - Application's state
 * @returns {Boolean} Laoding state for notes
 */
export const areNotesLoading = state => state.loading.notes;