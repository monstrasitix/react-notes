/**
 * Selectors are used to access state from Redux and allow easy meapping to your container
 * components which deal with side effects and isolate core functionality from the view.
 * I am using a very basic approach but it would be gread to add a more suffisticated abstraction
 * to provide constraints during test and desired implementations. - What if there are selectors
 * which are declared but never used? Redux requiers granularity and this may cause technical dept
 * in the end when you're creating mapping functions and using memo's for more general computations.
 * As React has it's own hierarchy it can also be said about Redux's store.
 * @module selectors
 */