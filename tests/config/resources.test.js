// Core
import Chai from 'chai';

// System Under Test
import ResourceConfiguration from '../../src/config/resources';


describe('Config/Resources', () => {
    it('should have API code passed in as a global variable from webpack', () => {
        // Assertions
        Chai.expect(ResourceConfiguration.note).to.haveOwnProperty('code', global.CODE);
        Chai.expect(ResourceConfiguration.note).to.haveOwnProperty('url');
    });
});