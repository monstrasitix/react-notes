// Core
import Chai from 'chai';

// System Under Test
import {
    ACTION,
    toggleSidebar
} from '../../src/actions/sidebar';


describe('Actions/Sidebar', () => {
    describe('Available actions', () => {
        it('should have properties named same as values', () => {
            // Assertions
            Object
                .entries(ACTION)
                .forEach(([property, value]) => (
                    Chai.expect(property).to.eq(value)
                ));
        });


        it('should allow following actions', () => {
            // Assertions
            Chai.expect(ACTION).to.have.property('SIDEBAR_TOGGLE');
        });
    });


    describe('loadingReplace', () => {
        it('should pass through assigned reducer', () => {
            // Setup
            const isOpen = true;
            // Execution
            const action = toggleSidebar(isOpen);
            // Assertions
            Chai.expect(action).to.eql({
                type: ACTION.SIDEBAR_TOGGLE,
                payload: isOpen,
            });
        });
    });
});