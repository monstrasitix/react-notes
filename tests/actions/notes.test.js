// Core
import Chai from 'chai';

// System Under Test
import {
    SAGA,
    ACTION,
    createNote,
    loadNotes,
    noteAdd,
    noteReplace,
    notesReplace,
    saveNote,
} from '../../src/actions/notes';


describe('Actions/Notes', () => {
    describe('Available actions', () => {
        it('should have properties named same as values', () => {
            // Assertions
            Object
                .entries(ACTION)
                .forEach(([property, value]) => (
                    Chai.expect(property).to.eq(value)
                ));
        });


        it('should allow following actions', () => {
            // Assertions
            Chai.expect(ACTION).to.have.property('NOTES_REPLACE');
            Chai.expect(ACTION).to.have.property('NOTE_REPLACE');
            Chai.expect(ACTION).to.have.property('NOTE_ADD');
        });
    });


    describe('Available saga middlewares', () => {
        it('should have properties named same as values', () => {
            // Assertions
            Object
                .entries(SAGA)
                .forEach(([property, value]) => (
                    Chai.expect(property).to.eq(value)
                ));
        });


        it('should allow following actions', () => {
            // Assertions
            Chai.expect(SAGA).to.have.property('LOAD_NOTES');
            Chai.expect(SAGA).to.have.property('SAVE_NOTE');
            Chai.expect(SAGA).to.have.property('CREATE_NOTE');
        });
    });


    describe('notesReplace', () => {
        it('should pass through assigned reducer', () => {
            // Setup
            const notes = 'notes';
            // Execution
            const action = notesReplace(notes);
            // Assertions
            Chai.expect(action).to.eql({
                type: ACTION.NOTES_REPLACE,
                payload: notes,
            });
        });
    });

    describe('noteReplace', () => {
        it('should pass through assigned reducer', () => {
            // Setup
            const id = 'id';
            const note = 'note';
            // Execution
            const action = noteReplace(id, note);
            // Assertions
            Chai.expect(action).to.eql({
                type: ACTION.NOTE_REPLACE,
                payload: note,
                meta: id,
            });
        });
    });


    describe('noteAdd', () => {
        it('should pass through assigned reducer', () => {
            // Setup
            const note = 'note';
            // Execution
            const action = noteAdd(note);
            // Assertions
            Chai.expect(action).to.eql({
                type: ACTION.NOTE_ADD,
                payload: note,
            });
        });
    });


    describe('loadNotes', () => {
        it('should pass through assigned reducer', () => {
            // Execution
            const action = loadNotes();
            // Assertions
            Chai.expect(action).to.eql({
                type: SAGA.LOAD_NOTES,
            });
        });
    });


    describe('saveNote', () => {
        it('should pass through assigned reducer', () => {
            // Setup
            const id = 'id';
            const note = 'note';
            // Execution
            const action = saveNote(id, note);
            // Assertions
            Chai.expect(action).to.eql({
                id,
                note,
                type: SAGA.SAVE_NOTE,
            });
        });
    });


    describe('createNote', () => {
        it('should pass through assigned reducer', () => {
            // Setup
            const note = 'note';
            // Execution
            const action = createNote(note);
            // Assertions
            Chai.expect(action).to.eql({
                note,
                type: SAGA.CREATE_NOTE,
            });
        });
    });
});