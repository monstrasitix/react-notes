// Core
import Chai from 'chai';

// System Under Test
import {
    ACTION,
    loadingReplace,
} from '../../src/actions/loading';


describe('Actions/Loading', () => {
    describe('Available actions', () => {
        it('should have properties named same as values', () => {
            // Assertions
            Object
                .entries(ACTION)
                .forEach(([property, value]) => (
                    Chai.expect(property).to.eq(value)
                ));
        });


        it('should allow following actions', () => {
            // Assertions
            Chai.expect(ACTION).to.have.property('LOADING_REPLACE');
        });
    });


    describe('loadingReplace', () => {
        it('should pass through assigned reducer', () => {
            // Setup
            const entity = 'entity';
            const value = 'value';
            // Execution
            const action = loadingReplace(entity, value);
            // Assertions
            Chai.expect(action).to.eql({
                type: ACTION.LOADING_REPLACE,
                payload: value,
                meta: entity,
            });
        });
    });
});