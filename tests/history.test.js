// Core
import Chai from 'chai';

// System Under Test
import {
    getUserConfirmation
} from '../src/history';

describe('History', () => {
    describe('getUserConfirmation', () => {
        it('should execute a callback with dependency return value', () => {
            // Setup
            const passThrough = arg => arg;
            const message = ['message string'];
            // Execution
            const curried = getUserConfirmation(passThrough);
            const sut = curried(message, passThrough);
            // Assertions
            Chai.expect(curried).to.be.a('Function');
            Chai.expect(curried.length).to.eq(2);
            Chai.expect(sut).to.eql(message);
        });
    });
});