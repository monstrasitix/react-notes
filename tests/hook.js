require('@babel/register');
require('@babel/polyfill');
require('@babel/preset-react');
require('jsdom-global/register');

// Global API resource provided by Webpack is mocked
global.CODE = 'CODE';
global.fetch = (...args) => args;