// Core
import Chai from 'chai';

// System Under Test
import {
    getEnvironment
} from '../src/environment';

describe('Environment', () => {
    describe('getEnvironment', () => {
        it('should validate passed string for a particular environment', () => {
            // Assertions
            Chai.expect(getEnvironment('TEST')).to.have.property('TESTING', true);
            Chai.expect(getEnvironment('devs')).to.have.property('DEVELOPMENT', true);
            Chai.expect(getEnvironment('pRodUc')).to.have.property('PRODUCTION', true);
        });


        it('should return a schema with 3 available properties', () => {
            // Setup
            const expected = {
                TESTING: false,
                DEVELOPMENT: false,
                PRODUCTION: false,
            };
            const env = 'none';
            // Execution
            const sut = getEnvironment(env);
            // Assertions
            Chai.expect(sut).to.eql(expected);
        });
    });
});