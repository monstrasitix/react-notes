// Core
import Chai from 'chai';

// System Under Test
import {
    getNotes
} from '../../src/selectors/notes';


describe('Selectors/Notes', () => {
    describe('getNotes', () => {
        it('should return notes property', () => {
            // Setup
            const state = {
                notes: true,
            };
            // Assertion
            Chai.expect(getNotes(state)).to.eq(true);
        });
    });
});