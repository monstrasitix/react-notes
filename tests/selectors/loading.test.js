// Core
import Chai from 'chai';

// System Under Test
import {
    areNotesLoading
} from '../../src/selectors/loading';


describe('Selectors/Loading', () => {
    describe('areNotesLoading', () => {
        it('should return notes property', () => {
            // Setup
            const state = {
                loading: {
                    notes: true,
                }
            };
            // Assertion
            Chai.expect(areNotesLoading(state)).to.eq(true);
        });
    });
});