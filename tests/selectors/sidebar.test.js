// Core
import Chai from 'chai';

// System Under Test
import {
    isSidebarOpen
} from '../../src/selectors/sidebar';


describe('Selectors/Sidebar', () => {
    describe('isSidebarOpen', () => {
        it('should return sidebar isOpe property', () => {
            // Setup
            const state = {
                sidebar: {
                    isOpen: true
                },
            };
            // Assertion
            Chai.expect(isSidebarOpen(state)).to.eq(true);
        });
    });
});