// Core
import Chai from 'chai';

// System Under Test
import sagas, {
    addNote,
    saveNote,
    loadNotes,
} from '../../src/sagas/notes';


describe('Sagas/Notes', () => {
    describe('loadNotes', () => {
        it('should create saga', () => {
            // Setup
            const iterator = loadNotes();
            // Assertions
            iterator.next();
            iterator.next();
            iterator.next();
            Chai.expect(iterator.next().done).to.eq(true);
        });

        it('should create saga', () => {
            // Setup
            const iterator = loadNotes();
            // Assertions
            iterator.next();
            iterator.next(() => {
                throw new Error('Some error');
            });
            iterator.next();
            Chai.expect(iterator.next().done).to.eq(true);
        });
    });


    describe('addNote', () => {
        it('should create saga', () => {
            // Setup
            const iterator = addNote({
                note: {
                    id: 5
                }
            });
            // Assertions
            iterator.next();
            iterator.next();
            iterator.next();
            Chai.expect(iterator.next().done).to.eq(true);
        });
    });


    describe('saveNote', () => {
        it('should create saga', () => {
            // Setup
            const iterator = saveNote({
                id: 1,
                note: {}
            });
            // Assertions
            iterator.next();
            iterator.next();
            iterator.next();
            Chai.expect(iterator.next().done).to.eq(true);
        });
    });
});