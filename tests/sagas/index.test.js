// Core
import Chai from 'chai';

// System Under Test
import sagas from '../../src/sagas';


describe('Sagas', () => {
    it('should create saga', () => {
        // Setup
        const iterator = sagas();
        // Assertions
        Chai.expect(iterator.next().done).to.eq(false);
        Chai.expect(iterator.next().done).to.eq(true);
    });
});