// Core
import Chai from 'chai';

// System Under Test
import {
    fonts,
    fontMap,
    fontValues,
    headingValues,
    fontValueMap,
    transformer,
} from '../../src/modules/editor';


describe('Modules/Editor', () => {
    describe('fonts', () => {
        it('should have font properties with appropriate font names', () => {
            // Setup
            const validator = ([property, value]) => {
                Chai.expect(property).to.eq(transformer(value));
            };
            // Assertions
            Object
                .entries(fonts)
                .forEach(validator);
        });
    });


    describe('headingValues', () => {
        it('should have elements with value and text properties', () => {
            // Setup
            const validator = (heading) => {
                Chai.expect(heading).to.have.property('value');
                Chai.expect(heading).to.have.property('text');
            };
            // Assertions
            headingValues.forEach(validator);
        });
    });


    describe('fontMap', () => {
        it('should iterate through every font and assign value as font-family', () => {
            // Setup
            const validator = ([property, value]) => {
                Chai
                    .expect(fontMap[property])
                    .to.have.property('fontFamily', value);
            };
            // Asertions
            Object
                .entries(fonts)
                .forEach(validator);
        });
    });


    describe('fontValues', () => {
        it('should have elements mapped to value and text properties', () => {
            const validator = (heading) => {
                Chai.expect(heading).to.have.property('value');
                Chai.expect(heading).to.have.property('text');
            };
            // Assertions
            fontValues.forEach(validator);
        });
    });


    describe('fontValueMap', () => {
        it('should map two array elements to object properties', () => {
            // Setup
            const element = [1, 2];
            // Execution
            const sut = fontValueMap(element);
            // Assertions
            Chai.expect(sut).to.eql({
                value: element[0],
                text: element[1],
            });
        });
    });
});