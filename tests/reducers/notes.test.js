// Core
import Chai from 'chai';
import {
    createStore
} from 'redux';

// System Under Test
import reducer, {
    initialState
} from '../../src/reducers/notes';
import {
    notesReplace,
    noteReplace,
    noteAdd,
} from '../../src/actions/notes';


describe('Reducers/Notes', () => {
    it('should have this initialState', () => {
        // Assertions
        Chai.expect(initialState).to.eql([]);
    });


    describe('Reducer', () => {
        let store;

        beforeEach(() => {
            store = createStore(reducer, initialState);
        });

        it('should replace property with payload', () => {
            // Setup
            const notes = [];
            const action = notesReplace(notes);
            // Execution
            store.dispatch(action);
            const newState = store.getState();
            // Assertions
            Chai.expect(newState).to.eql(notes);
        });


        it('should replace element with matching identifier', () => {
            // Setup
            const id = 1;
            const newNotes = [{
                    id: 1,
                    title: '1'
                },
                {
                    id: 2,
                    title: '2'
                },
            ];
            const note = {
                id,
                title: 'New Note'
            };
            const action = noteReplace(id, note);
            // Execution
            newNotes.map(newNote => store.dispatch(noteAdd(newNote)));
            store.dispatch(action);
            const newState = store.getState();
            // Assertions
            Chai.expect(newState).to.eql([
                note,
                newNotes[1],
            ]);
        });


        it('should return initial state when aciton does not match', () => {
            // Setup
            const action = {
                type: 'NOOP'
            };
            // Execution
            store.dispatch(action);
            const newState = store.getState();
            // Assertions
            Chai.expect(newState).to.eql(initialState);
        });

        it('should default to initialState', () => {
            // Assertion
            Chai.expect(reducer(undefined, {})).to.eql(initialState);
        });
    });
});