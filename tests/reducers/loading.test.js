// Core
import Chai from 'chai';
import { createStore } from 'redux';

// System Under Test
import reducer, {
    initialState
} from '../../src/reducers/loading';
import {
    loadingReplace
} from '../../src/actions/loading';


describe('Reducers/Loading', () => {
    it('should have this initialState', () => {
        // Assertions
        Chai.expect(initialState).to.eql({
            notes: false,
        });
    });


    describe('Reducer', () => {
        let store;

        beforeEach(() => {
            store = createStore(reducer, initialState);
        });

        it('should append meta property with payload value', () => {
            // Setup
            const value = 'value';
            const entity = 'entity';
            const action = loadingReplace(entity, value);
            // Execution
            store.dispatch(action);
            const newState = store.getState();
            // Assertions
            Chai.expect(newState).to.eql({
                ...initialState,
                [entity]: value,
            });
        });


        it('should return initial state when aciton does not match', () => {
            // Setup
            const action = { type: 'NOOP' };
            // Execution
            store.dispatch(action);
            const newState = store.getState();
            // Assertions
            Chai.expect(newState).to.eql(initialState);
        });


        it('should default to initialState', () => {
            // Assertion
            Chai.expect(reducer(undefined, { })).to.eql(initialState);
        });
    });
});