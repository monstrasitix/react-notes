// Core
import Chai from 'chai';
import {
    createStore
} from 'redux';

// System Under Test
import reducer, {
    initialState
} from '../../src/reducers/sidebar';
import {
    toggleSidebar
} from '../../src/actions/sidebar';


describe('Reducers/Sidebar', () => {
    it('should have this initialState', () => {
        // Assertions
        Chai.expect(initialState).to.eql({
            isOpen: true,
        });
    });


    describe('Reducer', () => {
        let store;

        beforeEach(() => {
            store = createStore(reducer, initialState);
        });

        it('should replace property with payload', () => {
            // Setup
            const isOpen = false;
            const action = toggleSidebar(isOpen);
            // Execution
            store.dispatch(action);
            const newState = store.getState();
            // Assertions
            Chai.expect(newState).to.eql({
                isOpen
            });
        });


        it('should return initial state when aciton does not match', () => {
            // Setup
            const action = {
                type: 'NOOP'
            };
            // Execution
            store.dispatch(action);
            const newState = store.getState();
            // Assertions
            Chai.expect(newState).to.eql(initialState);
        });


        it('should default to initialState', () => {
            // Assertion
            Chai.expect(reducer(undefined, { })).to.eql(initialState);
        });
    });
});